export interface GameBoardSquare {
  letter: string;
  isLocked?: boolean;
}
